var gameApp = window.gameApp || {};

gameApp = (function() {
    var $el = $('.game');
    var $startBtn = $el.find('.game-btn-start');
    var $reloadBtn = $el.find('.game-btn-reload');
    var $introLayer = $el.find('.game-layer-intro');
    var $cardsLayer = $el.find('.game-layer-cards');
    var $resultLayer = $el.find('.game-layer-result');
    var $steps = $el.find('.game-data-steps');
    var $cards = $el.find('.game-cards');
    var $result = $resultLayer.find('.game-result-desc');
    var $resultSteps = $resultLayer.find('.game-result-steps');
    var $loader = $el.find('.loader');
    var $share = $el.find('.share > div');
    var baseUrl;
    var cards = [
        {
            photo: 'https://b1.m24.ru/c/1183807.580xp.jpg',
            photoAuthor: '',
            title: 'Кристиан Бейл',
            desc: 'Номинирован на «Оскар» за яркий и убедительный портрет Дика Чейни — самого влиятельного вице-президента в истории США (фильм «Власть»)'
        },
        {
            photo: 'https://b1.m24.ru/c/1183793.580xp.jpg',
            photoAuthor: '',
            title: 'Гленн Клоуз',
            desc: 'Номинирована на «Оскар» за роль Джоан — отчаянной домохозяйки с писательским амбициями, слишком любящей своего мужа (фильм «Жена»)'
        },
        {
            photo: 'https://b1.m24.ru/c/1183796.580xp.jpg',
            photoAuthor: '',
            title: 'Адам Драйвер',
            desc: 'Номинирован на «Оскар» за роль Флипа — поразительно уравновешенного детектива и члена Ку-клукс-клана под прикрытием (фильм «Чёрный клановец»)'
        },
        {
            photo: 'https://b1.m24.ru/c/1183795.580xp.jpg',
            photoAuthor: '',
            title: 'Леди Гага',
            desc: 'Номинирована на «Оскар» за роль Элли — амбициозной и страстной молодой артистки (фильм «Звезда родилась»)'
        },
        {
            photo: 'https://b1.m24.ru/c/1183794.580xp.jpg',
            photoAuthor: '',
            title: 'Уиллем Дефо',
            desc: 'Номинирован на «Оскар» за роль Винсента ван Гога — сумасшедшего гения, мечтающего когда-нибудь стать знаменитым художником (фильм «Ван Гог. На пороге вечности»)'
        },
        {
            photo: 'https://b1.m24.ru/c/1183792.580xp.jpg',
            photoAuthor: '',
            title: 'Реджина Кинг',
            desc: 'Номинирована на «Оскар» за роль Шэрон — матери, мечтающей о счастье для дочери, которая стала заложницей обстоятельств (в фильме «Если Бил-стрит могла бы заговорить»)'
        },
        {
            photo: 'https://b1.m24.ru/c/1183791.580xp.jpg',
            photoAuthor: '',
            title: 'Сэм Рокуэлл',
            desc: 'Номинирован на «Оскар» за почти безупречный портрет Джорджа Буша-младшего, 43-го президента США (фильм  «Власть»)'
        },
        {
            photo: 'https://b1.m24.ru/c/1183797.580xp.jpg',
            photoAuthor: '',
            title: 'Эми Адамс',
            desc: 'Номинирована на «Оскар» за роль Линн Чейни — супруги экс-вице-президента США (фильм  «Власть»)'
        }
    ];

    // $share.attr('data-url', location.href)
    // $share.attr('data-image', 'https://b1.m24.ru/c/1184080.jpg')

    var cardsQnt = cards.length * 2;
    var step = 0;
    var createNewPLayer = function() {
        initCards();
    };
    function declOfNum(n, titles) {
        return titles[n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2];
    }
    var url = '../images/oscars3.svg'
    var initCards = function() {
        var randomCards = cards.slice(0);
        var cardsHTML = '';
        function shuffle(a) {
            var j, x, i;
            for (i = a.length - 1; i > 0; i--) {
                j = Math.floor(Math.random() * (i + 1));
                x = a[i];
                a[i] = a[j];
                a[j] = x;
            }
        }
        for (var i = 0, l = cards.length; i < l; i++) {
            randomCards.push(randomCards[i]);
        }
        shuffle(randomCards);
        for (var i = 0, l = randomCards.length; i < l; i++) {
            cardsHTML += '<div class="game-cards-item">' +
                                '<div class="game-card" data-photo="' + randomCards[i]['photo'] + '" data-photo-author="' + randomCards[i]['photoAuthor'] + '" data-title="' + randomCards[i]['title'] + '" data-desc="' + randomCards[i]['desc'] + '">' +
                                    '<div class="game-card-side game-card-cover"></div>' +
                                    '<div class="game-card-side game-card-photo" style="background-image: url(' + randomCards[i]['photo'] + ')"></div>' +
                                '</div>' +
                            '</div>';
            if (i == (randomCards.length - 1)) {
                $cards.html(cardsHTML);
                $el.addClass('is-start');
            }
        }
    };
    var selectCard = function(card) {
        var previous = $cards.find('.active');
        if (!card.hasClass('active') && !card.hasClass('done') && previous.length < 2) {
            card.addClass('active');
            if (previous.length) {
                var photo = card.data('photo');
                var photoAuthor = card.data('photo-author');
                var title = card.data('title');
                var desc = card.data('desc');
                if (previous.data('title') === title) {
                    previous.addClass('done');
                    card.addClass('done');
                    setTimeout(function() {
                        showDesc(photo, photoAuthor, title, desc);
                    }, 500);
                }
                setTimeout(function() {
                    $steps.text(++step);
                    previous.removeClass('active');
                    card.removeClass('active');
                }, 1000);
            }
        }
    };
    var showDesc = function(photo, photoAuthor, title, desc) {
        var descPopup = $('<div class="game-cards-desc">' +
                            '<div class="game-cards-desc-photo" style="background-image:url(' + photo + ')"></div>' +
                            '<div class="game-cards-desc-photo-author">' + photoAuthor + '</div>' +
                            '<h3>' + title + '</h3>' +
                            '<p>' + desc + '</p>' +
                            '<div class="game-cards-desc-btn"><button class="game-btn">Продолжить</button></div>' +
                        '</div>');
        $el.addClass('popup-visible').append(descPopup);
    };
    var showResult = function() {
        $el.addClass('is-complete');
        $resultSteps.html('<b>' + step + '</b>' + declOfNum(step, [' ход', ' хода', ' ходов']));
    };
    var init = function(options) {
        baseUrl = options.baseUrl;
        $el.show();
        $startBtn.on('click', function(e) {
            $steps.text(0);
            createNewPLayer();
            // $share.attr('data-image', '')
        });
        $cards.on('click', '.game-card', function(e) {
            var self = $(this);
            selectCard(self);
        });
        $('body').on('click', '.game-cards-desc button', function() {
            var self = $(this);
            var completedCardsQnt = $cards.find('.done').length;
            self.parents('.game-cards-desc').remove();
            $el.removeClass('popup-visible');
            if (completedCardsQnt === cardsQnt) {
                showResult();
            }
        });
        $reloadBtn.on('click', function(e) {
            $el.removeClass('is-complete');
            step = 0;
            $steps.text(step);
            $cards.empty();
            // $share.attr('data-image', 'http://media.interactive.netuse.gr/filesystem/images/20190116/low/aeg-still-0369_1581_107712670.JPG')
            initCards();
        });
    };

    return {
        init: init
    };
})();

$(function() {
    gameApp.init({
        baseUrl: ''
    });
});
